<?php

namespace PhpDevOrg;
class Image
{
    public
        $name,
        $dir,
        $path,
        $src,
        $width,
        $height,
        $newWidth,
        $newHeight,
        $resizeCoefficient,
        $extension,
        $quality,
        $doResize = false,
        $imgType, // image || waterMark
        $error = array(),
        //only waterMark
        $autoResize,
        $autoResizePercent,
        $position,
        $position_X = 0,
        $position_Y = 0,
        $indentType,
        $indentLeft,
        $indentTop,
        $indentRight,
        $indentBottom,
        //settings
        $debug = true,
        $maxResizeWidth = 1600,
        $maxResizeHeight = 1600,
        $maxResizeCoefficient = 6;

    function __construct($arImage, $imgType)
    {
        //get name
        if(!empty($arImage['name'])){
            $this->name = $this->getName($arImage['name']);
            $this->dir = $this->getDir($arImage['name']);
            $this->src = $this->dir.'/'.$this->name;
        }
        else{
            $this->error[] = 'File name not specified.';
            return $this;
        }

        //check file
        $this->path = $this->getAbsolutePath();
        if(!file_exists($this->path)){
            $this->error[] = 'File "' . $this->name . '" not found.';
            return $this;
        }

        //set params
        $this->imgType = $imgType;
        $this->extension = $this->getExtension($this->name);
        $this->setOptionsForResize($arImage);
        $this->setSize();
        //if(!empty($arImage['quality']))
        //    $this->quality = intval($arImage['quality']);

        if($this->imgType === 'waterMark'){
            if(!empty($arImage['auto_resize'])){
                $this->autoResize = true;
                $this->autoResizePercent = intval($arImage['auto_resize']);
            }

            if(!empty($arImage['position']))
                $this->position = $arImage['position'];

            $this->setIndent($arImage);
        }

        //copy
        if(!empty($arImage['copy']) || $this->imgType === 'waterMark'){
            $newName = $this->makeNewName();
            if($this->copyFile($newName))
                $this->setNewName($newName);
            else
                $this->error[] = 'Could not copy file '.$this->name.'.';
        }
    }

    function __destruct()
    {
        if($this->debug === true)
            $this->debug2console();
    }

    public static function addWaterMark($arImage, $arWaterMark, $quality = 75)
    {
        $image = new self($arImage, 'image');
        $waterMark = new self($arWaterMark, 'waterMark');

        if(!empty($image->error) || !empty($waterMark->error)){
            $arError['error'] = array_merge($image->error, $waterMark->error);
            return $arError;
        }

        //resize
        if($image->doResize === true)
            $image->resize();

        if($waterMark->doResize === true){
            if($waterMark->autoResize === true)
                $waterMark->setSizeForResizeWaterMark($image);

            $waterMark->resize();
        }

        // Loading stamp and picture
        $img = self::imageCreateFrom($image);
        $stamp = self::imageCreateFrom($waterMark);


        // Set width/height picture and stamp
        $stampWidth = imagesx($stamp);
        $stampHeight = imagesy($stamp);
        $imgWidth = imagesx($img);
        $imgHeight = imagesy($img);


        $waterMark->setPosition($imgWidth, $imgHeight);


        // !!! I do not understand why, but this code works correctly (for resize)
        $imgWidth = $stampWidth;
        $imgHeight = $stampHeight;
        //


        imagecopyresampled(
            $img,
            $stamp,
            $waterMark->position_X,
            $waterMark->position_Y,
            0,
            0,
            $imgWidth,
            $imgHeight,
            $stampWidth,
            $stampHeight
        );
        imagedestroy($stamp);
        self::saveImage($img, $image, $quality);

        $arResult = array(
            'src' => $image->src,
        );

        return $arResult;
    }



    private static function saveImage($img, self $image, $quality = 75)
    {
        unlink($image->path);

        //if(!empty($image->quality))
        //    $quality = $image->quality;

        if($image->extension === 'jpg' || $image->extension === 'jpeg')
            imagejpeg($img, $image->path, $quality);
        elseif($image->extension === 'png'){
            $pngQuality = 9 - round(($quality / 100 ) * 9 );
            imagepng($img, $image->path, $pngQuality);
        }


        imagedestroy($img);
        if(file_exists($image->path))
            return true;
        return false;
    }

    private static function imageCreateFrom(self $image){
        if($image->extension === 'jpg' || $image->extension === 'jpeg')
            $img = imagecreatefromjpeg($image->path);
        elseif($image->extension === 'png')
            $img = imagecreatefrompng($image->path);
        else
            $image->error[] = 'Invalid file extension ('.$image->name.')';

        return $img;
    }

    private function resize(){
        $imgOld = $this->imageCreateFrom($this);
        $img_w = imagesx($imgOld);
        $img_h = imagesy($imgOld);


        if(!empty($this->newWidth) && !empty($this->newHeight)){
            $newImg_w = $this->newWidth;
            $newImg_h = $this->newHeight;
        }
        else if(!empty($this->resizeCoefficient)){
            $newImg_w = $img_w * $this->resizeCoefficient;
            $newImg_h = $img_h * $this->resizeCoefficient;
        }

        if($newImg_w > $this->maxResizeWidth)
            $newImg_w = $this->maxResizeWidth;
        if($newImg_h > $this->maxResizeHeight)
            $newImg_h = $this->maxResizeHeight;


        $img = imagecreatetruecolor($newImg_w, $newImg_h);
        $bg = imagecolorallocate($img, 0, 0, 0);
        imagecolortransparent($img, $bg);
        imagealphablending($img, false);
        imagesavealpha($img, true);

        imagecopyresampled($img, $imgOld,0,0,0,0, $newImg_w, $newImg_h, $img_w, $img_h);
        imagedestroy($imgOld);

        if($this->saveImage($img, $this)){
            $this->setSize();
            return true;
        }
        else
            $this->error[] = 'Could not create file "'.$this->name.'".';
        return false;
    }

    private function getAbsolutePath($newName = null){
        $name = $this->name;
        if(!empty($newName))
            $name = $newName;
        return $_SERVER['DOCUMENT_ROOT'].$this->dir.'/'.$name;
    }

    private function getExtension($name){
        return pathinfo($name, PATHINFO_EXTENSION);
    }

    private function getDir($name){
        $dir = pathinfo($name, PATHINFO_DIRNAME);
        if($dir === '.' || $dir === '\\')
            $dir = '';
        return $dir;
    }

    private function getName($name){
        return pathinfo($name, PATHINFO_BASENAME);
    }

    private function debug2console($data = null)
    {
        if(empty($data))
            $data = $this;
        echo '<script type="text/javascript">console.log('.json_encode($data).');</script>';
    }

    private function copyFile($newName){
        $newFile = $this->getAbsolutePath($newName);
        if(copy($this->path, $newFile))
            return true;
        return false;
    }

    private function makeNewName($newName = null){
        if(!empty($newName)){
            $name = pathinfo($newName, PATHINFO_FILENAME);
            $result = $name . '.' . $this->extension;
        }
        else
            $result = 'copy_'.$this->name;
        return $result;
    }

    private function setOptionsForResize($arImage)
    {
        if((!empty($arImage['width']) || !empty($arImage['w'])) && (!empty($arImage['height']) || !empty($arImage['h']))){
            $this->newWidth = !empty($arImage['width']) ? intval($arImage['width']) : intval($arImage['w']);
            $this->doResize = true;

            $this->newHeight = !empty($arImage['height']) ? intval($arImage['height']) : intval($arImage['h']);
            $this->doResize = true;
        }

        if(!empty($arImage['resize_coefficient'])){
            $this->resizeCoefficient = +$arImage['resize_coefficient'];

            if($this->resizeCoefficient > $this->maxResizeCoefficient)
                $this->resizeCoefficient = $this->maxResizeCoefficient;
            if($this->resizeCoefficient < 0.1)
                $this->resizeCoefficient = 0.1;

            $this->doResize = true;
        }

        if(!empty($arImage['auto_resize']) && $this->imgType === 'waterMark'){
            $this->autoResize = true;
            $this->doResize = true;
        }
    }

    private function setNewName($newName){
        $this->path = str_replace($this->name, $newName, $this->path);
        $this->src = str_replace($this->name, $newName, $this->src);
        $this->name = $newName;
    }

    private function setSizeForResizeWaterMark(self $image){
        $newWidth = $image->width / 100 * $this->autoResizePercent;
        $this->resizeCoefficient = $newWidth / $this->width;
    }

    private function setSize(){
        $arSize = getimagesize($this->path);
        $this->width = $arSize[0];
        $this->height = $arSize[1];
    }

    private function setPosition($imgWidth, $imgHeight){

        if($this->indentType === 'pixel'){

            if(!empty($this->indentLeft))
                $this->position_X = $this->indentLeft;
            else if(!empty($this->indentRight))
                $this->position_X = $imgWidth - $this->indentRight - $this->width;

            if(!empty($this->indentTop))
                $this->position_Y = $this->indentTop;
            else if(!empty($this->indentBottom))
                $this->position_Y = $imgHeight - $this->indentRight - $this->height;
        }
        else if($this->indentType === 'percent'){

            if(!empty($this->indentLeft))
                $this->position_X = $imgWidth * $this->indentLeft / 100;
            else if(!empty($this->indentRight))
                $this->position_X = $imgWidth - ($imgWidth * $this->indentRight / 100) - $this->width;

            if(!empty($this->indentTop))
                $this->position_Y = $imgHeight * $this->indentTop / 100;
            else if(!empty($this->indentBottom))
                $this->position_Y = $imgHeight - ($imgHeight * $this->indentBottom / 100) - $this->height;
        }
        else if(!empty($this->position)){

            if($this->position === 'left' || $this->position === 'l')
                $this->position_Y = $imgHeight - ($imgHeight / 2 + $this->height / 2);
            else if($this->position === 'top' || $this->position === 't')
                $this->position_X = $imgWidth - ($imgWidth / 2 + $this->width / 2);
            else if($this->position === 'right' || $this->position === 'r'){
                $this->position_Y = $imgHeight - ($imgHeight / 2 + $this->height / 2);
                $this->position_X = $imgWidth - $this->width;
            }
            else if($this->position === 'bottom' || $this->position === 'b'){
                $this->position_Y = $imgHeight - $this->height;
                $this->position_X = $imgWidth - ($imgWidth / 2 + $this->width / 2);
            }
            else if($this->position === 'center' || $this->position === 'c'){
                $this->position_Y = $imgHeight - ($imgHeight / 2 + $this->height / 2);
                $this->position_X = $imgWidth - ($imgWidth / 2 + $this->width / 2);
            }
            else if($this->position === 'topleft' || $this->position === 'tl'){
                $this->position_Y = 0;
                $this->position_X = 0;
            }
            else if($this->position === 'topright' || $this->position === 'tr'){
                $this->position_Y = 0;
                $this->position_X = $imgWidth - $this->width;
            }
            else if($this->position === 'bottomleft' || $this->position === 'bl'){
                $this->position_Y = $imgHeight - $this->height;
                $this->position_X = 0;
            }
            else if($this->position === 'bottomright' || $this->position === 'br'){
                $this->position_Y = $imgHeight - $this->height;
                $this->position_X = $imgWidth - $this->width;
            }
        }
    }

    private function setIndent($arImage){
        $indent = null;
        if(!empty($arImage['left'])){
            $indent = $arImage['left'];
            $this->indentLeft = intval($arImage['left']);
        }
        else if(!empty($arImage['l'])){
            $indent = $arImage['l'];
            $this->indentLeft = intval($arImage['l']);
        }
        else if(!empty($arImage['right'])){
            $indent = $arImage['right'];
            $this->indentRight = intval($arImage['right']);
        }
        else if(!empty($arImage['r'])){
            $indent = $arImage['r'];
            $this->indentRight = intval($arImage['r']);
        }

        if(!empty($arImage['top'])){
            $indent = $arImage['top'];
            $this->indentTop = intval($arImage['top']);
        }
        else if(!empty($arImage['t'])){
            $indent = $arImage['t'];
            $this->indentTop = intval($arImage['t']);
        }
        else if(!empty($arImage['bottom'])){
            $indent = $arImage['bottom'];
            $this->indentBottom = intval($arImage['bottom']);
        }
        else if(!empty($arImage['b'])){
            $indent = $arImage['b'];
            $this->indentBottom = intval($arImage['b']);
        }

        if(!empty($indent) && substr($indent, (strlen($indent) - 1)) === '%')
            $this->indentType = 'percent';
        else if(!empty($indent))
            $this->indentType = 'pixel';
    }
}